`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/06 11:18:22
// Design Name: 
// Module Name: DSP_ALU_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_tb;

	reg [31:0] src0;
	reg [31:0] src1;
	reg clk;
	reg [6:0] op;
	reg [3:0] alu;
	reg icf;
	wire [31:0] dst;
	wire ozf;
	wire ocf;

	ALU uut (
		.src0(src0),
		.src1(src1),
		.clk(clk),
		.op(op),
		.alu(alu),
		.icf(icf),
		.dst(dst),
		.ozf(ozf),
		.ocf(ocf)
	);
	
	always begin
		#5 clk <= ~clk;
	end
	
	initial begin
		src0 <= 32'h00000000;
		src1 <= 32'h00000000;
		clk <= 1'b1;
		op <= 7'b0110011;
		alu <= 4'b1100;		// 0000:Z+X+Y+CIN 0011:Z-X-Y-CIN 0100:X^Z 1100:X&Z
		icf <= 1'b0;
		
//									op		alu
//			inc  : Z+X+Y+CIN		0110000	0000	(X=0, Y=0, CIN=1)
//			dec  : Z-X-Y-CIN		0110000	0011	(X=0, Y=0, CIN=1)
//			add  : Z+X+Y+CIN		0110011	0000	(Y=0, CIN=0)
//			adc  : Z+X+Y+CIN		0110011	0000	(Y=0, CIN=CF)
//			sub  : Z-X-Y-CIN		0110011	0011	(Y=0, CIN=0)
//			sbc  : Z-X-Y-CIN		0110011	0011	(Y=0, CIN=CF)
//			neg  : -Z+X+Y+CIN-1		0110000 0001	(X=0, CIN=1)
//			not  : Z~^X				0111000	0100	(X=0, Y=48'hffffffffffff)
//			or   : Z|X				0111011	1100	(Y=48'hffffffffffff)
//			and  : Z&X				0110011	1100	(Y=0)
//			xor  : Z^X				0110011	0100	(Y=0)
		#200
		src0 <= 32'h21524110;
		src1 <= 32'h00020001;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		#10
		src0 <= 32'h00020001;
		src1 <= 32'h12345678;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		#10
		src0 <= 32'h12345678;
		src1 <= 32'hdcba9876;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		#10
		src0 <= 32'hdeadbeef;
		src1 <= 32'h12345678;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		#10
		src0 <= 32'hffffffff;
		src1 <= 32'hffffffff;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		#10
		src0 <= 32'h00000000;
		src1 <= 32'h00000000;
			op <= 7'b0110000;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0000;	icf = 1'b1;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0011;	icf = 1'b1;
		#10	op <= 7'b0110000;	alu <= 4'b0001;	icf = 1'b1;
		#10	op <= 7'b0111000;	alu <= 4'b0100;	icf = 1'b0;
		#10	op <= 7'b0111011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b1100;	icf = 1'b0;
		#10	op <= 7'b0110011;	alu <= 4'b0100;	icf = 1'b0;
		
		#30
			$finish;
	end
	
endmodule
