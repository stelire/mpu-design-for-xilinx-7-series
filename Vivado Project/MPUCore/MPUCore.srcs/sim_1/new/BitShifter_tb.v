`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/01/28 20:22:41
// Design Name: 
// Module Name: BitShifter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BitShifter_tb;

	reg [31:0] src;
	reg [4:0] sft;
	reg [2:0] op;
	reg icf;
	wire [31:0] dst;
	wire ocf;

	BitShifter uut (
		.src(src),
		.sft(sft),
		.op(op),
		.icf(icf),
		.dst(dst),
		.ocf(ocf)
	);
	
	initial begin
		src <= 32'h00000000;
		sft <= 5'h00;
		op <= 3'b000;
		icf = 1;
		#10
		src <= 32'habcdef78;
		sft <= 5'h4;
		op <= 3'b000;
		#10
		op <= 3'b100;
		#10
		op <= 3'b001;
		#10
		op <= 3'b101;
		#10
		op <= 3'b010;
		#10
		op <= 3'b110;
		#10
		op <= 3'b011;
		#10
		op <= 3'b111;
		#100
		$finish;
	end
	
endmodule
