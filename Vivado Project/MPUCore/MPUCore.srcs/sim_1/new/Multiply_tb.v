`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/06 11:18:22
// Design Name: 
// Module Name: DSP_ALU_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Multiply_tb;

	reg [31:0] src0;
	reg [31:0] src1;
	reg clk;
	wire [63:0] dst;
	wire ozf;

	Multiply uut (
		.src0(src0),
		.src1(src1),
		.clk(clk),
		.dst(dst),
		.ozf(ozf)
	);
	
	always begin
		#5 clk <= ~clk;
	end
	
	initial begin
		src0 <= 32'h00000000;
		src1 <= 32'h00000000;
		clk <= 1'b1;
		
		#200
		src0 <= 32'h00020001;
		src1 <= 32'h21524110;
		#10
		src0 <= 32'h00020001;
		src1 <= 32'hdcba9876;
		#10
		src0 <= 32'h12345678;
		src1 <= 32'h12345678;
		#10
		src0 <= 32'hdeadbeef;
		src1 <= 32'h12345678;
		#10
		src0 <= 32'hffffffff;
		src1 <= 32'hffffffff;
		#10
		src0 <= 32'h00000000;
		src1 <= 32'h00000000;
		
		#30
			$finish;
	end
	
endmodule
