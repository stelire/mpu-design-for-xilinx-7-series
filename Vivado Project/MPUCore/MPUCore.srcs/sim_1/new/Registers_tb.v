`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/10 01:27:42
// Design Name: 
// Module Name: Registers_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Registers_tb;

	reg [4:0] regw;
	reg [4:0] regr0;
	reg [4:0] regr1;
	reg [4:0] regr2;
	reg [31:0] in;
	reg clk;
	reg we;
	reg swap;
	wire [31:0] out0;
	wire [31:0] out1;
	wire [31:0] out2;

	Registers uut (
		.regw(regw),
		.regr0(regr0),
		.regr1(regr1),
		.regr2(regr2),
		.in(in),
		.clk(clk),
		.we(we),
		.swap(swap),
		.out0(out0),
		.out1(out1),
		.out2(out2)
	);
	
	integer i;
	
	always begin
		#5 clk <= ~clk;
	end
	
	initial begin
		clk <= 1'b1;
		we <= 1'b0;
		swap <= 1'b0;
		regw <= 5'h00;
		regr0 <= 5'h00;
		regr1 <= 5'h04;
		regr2 <= 5'h08;
		in <= 32'h00000000;
		
		#89
		for(i=0; i<32; i=i+1) begin
			#10
			we <= 1'b1;
			regw <= i;
			in <= 32'h01010101 * (31 - i);
		end
		
		#10
		we <= 1'b0;
		for(i=0; i<32; i=i+3) begin
			#3
			regr0 <= i;
			regr1 <= i+1;
			regr2 <= i+2;
		end
		
		#17
		swap <= 1'b1;
		for(i=0; i<32; i=i+3) begin
			#3
			regr0 <= i;
			regr1 <= i+1;
			regr2 <= i+2;
		end
		
		#30
		$finish;
	end
endmodule
