`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Satoshi Nakamura
// 
// Create Date: 2019/01/29 18:45:00
// Design Name: 32bit BitShifter
// Module Name: BitShifter
// Project Name: MPU Design for Xilinx 7 Series
// Target Devices: Xilinx Artix 7
// Tool Versions: Vivado 2016.3
// Description: 
// 		32bit Bit Shifter
// 		
// 			op
// 			000	Logical Shift Left
// 			001	Logical Shift Left
// 			010	Rotate Left
// 			011	Rotate Left with Carry Flag
// 			100	Logical Shift Right
// 			101	Arithmetic Shift Right
// 			110	Rotate Right
// 			111	Rotate Right with Carry Flag
// 		
// 		
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BitShifter(
	input wire [31:0] src,
	input wire [4:0] sft,
	input wire [2:0] op,
	input wire icf,
	output wire [31:0] dst,
	output wire ocf
	);
	
	wire [65:0] frame;
	wire [65:0] tmp;
	
	assign frame =	(op == 3'b000) ?	{1'b0, src, 33'h000000000} :
					(op == 3'b001) ?	{1'b0, src, 33'h000000000} :
					(op == 3'b010) ?	{1'b0, src, src, 1'b0} :
					(op == 3'b011) ?	{1'b0, src, icf, src} :
					(op == 3'b100) ?	{33'h000000000, src, 1'b0} :
					(op == 3'b101) ?	{{33{src[31]}}, src, 1'b0} :
					(op == 3'b110) ?	{1'b0, src, src, 1'b0} :
										{src, icf, src, 1'b0};
	
	assign tmp = (op[2] == 1'b0) ?	(frame << sft) : (frame >> sft);
	assign dst = (op[2] == 1'b0) ?	tmp[64:33] : tmp[32:1];
	assign ocf = (op[2] == 1'b0) ?	tmp[65] : tmp[0];
	
endmodule
