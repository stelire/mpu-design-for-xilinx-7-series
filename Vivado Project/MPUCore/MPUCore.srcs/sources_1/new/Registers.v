`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Satoshi Nakamura
// 
// Create Date: 2019/02/10 00:58:35
// Design Name: 32bit Register * 32 with swap mode
// Module Name: Registers
// Project Name: MPU Design for Xilinx 7 Series
// Target Devices: Xilinx Artix 7
// Tool Versions: Vivado 2016.1
// Description: 
//		32bit register * 32
// 		Uses RAM32M * 16
// 		Register No.0 is not permitting to write.(It is fixed to 0)
// 		A registers set have swap mode. (r8-r15 <=> r16-r23)
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Registers(
	input wire [4:0] regw,
	input wire [4:0] regr0,
	input wire [4:0] regr1,
	input wire [4:0] regr2,
	input wire [31:0] in,
	input wire clk,
	input wire we,
	input wire swap,
	output wire [31:0] out0,
	output wire [31:0] out1,
	output wire [31:0] out2
	);
	
	wire twe;
	wire [4:0] tregw;
	wire [4:0] tregr0;
	wire [4:0] tregr1;
	wire [4:0] tregr2;
	
	assign twe = (regw != 5'h00) ? we : 1'h0;
	
	assign tregw[4] = (swap == 1'b0) ? regw[4] : regw[3];
	assign tregw[3] = (swap == 1'b0) ? regw[3] : regw[4];
	assign tregw[2] = regw[2];
	assign tregw[1] = regw[1];
	assign tregw[0] = regw[0];
	
	assign tregr0[4] = (swap == 1'b0) ? regr0[4] : regr0[3];
	assign tregr0[3] = (swap == 1'b0) ? regr0[3] : regr0[4];
	assign tregr0[2] = regr0[2];
	assign tregr0[1] = regr0[1];
	assign tregr0[0] = regr0[0];
	
	assign tregr1[4] = (swap == 1'b0) ? regr1[4] : regr1[3];
	assign tregr1[3] = (swap == 1'b0) ? regr1[3] : regr1[4];
	assign tregr1[2] = regr1[2];
	assign tregr1[1] = regr1[1];
	assign tregr1[0] = regr1[0];
	
	assign tregr2[4] = (swap == 1'b0) ? regr2[4] : regr2[3];
	assign tregr2[3] = (swap == 1'b0) ? regr2[3] : regr2[4];
	assign tregr2[2] = regr2[2];
	assign tregr2[1] = regr2[1];
	assign tregr2[0] = regr2[0];
	
	genvar i;
	generate
		for(i=0; i<32; i=i+2) begin : RegisterSet	
			RAM32M #(
				.INIT_A(64'h0000000000000000),
				.INIT_B(64'h0000000000000000),
				.INIT_C(64'h0000000000000000),
				.INIT_D(64'h0000000000000000)
			) Reg2bitx32 (
				.DOA(out0[i+1:i]),
				.DOB(out1[i+1:i]),
				.DOC(out2[i+1:i]),
				.DOD(),
				.ADDRA(tregr0),
				.ADDRB(tregr1),
				.ADDRC(tregr2),
				.ADDRD(tregw),
				.DIA(in[i+1:i]),
				.DIB(in[i+1:i]),
				.DIC(in[i+1:i]),
				.DID(in[i+1:i]),
				.WCLK(clk),
				.WE(twe)
			);
		end
	endgenerate
	
endmodule
