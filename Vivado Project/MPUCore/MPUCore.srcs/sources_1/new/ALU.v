`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Satoshi Nakamura
// 
// Create Date: 2019/02/11 11:15:29
// Design Name: 32bit ALU by DSP
// Module Name: ALU
// Project Name: MPU Design for Xilinx 7 Series
// Target Devices: Xilinx Artix 7
// Tool Versions: Vivado 2016.3
// Description: 
// 		32bit Arithmetic Logic Unit
// 		Uses DSP48E1
// 		input flag : Carry flag
// 		output flag : Zero flag, Carry flag
//									op		alu
//			inc  : Z+X+Y+CIN		0110000	0000	(X=0, Y=0, CIN=1)
//			dec  : Z-X-Y-CIN		0110000	0011	(X=0, Y=0, CIN=1)
//			add  : Z+X+Y+CIN		0110011	0000	(Y=0, CIN=0)
//			adc  : Z+X+Y+CIN		0110011	0000	(Y=0, CIN=CF)
//			sub  : Z-X-Y-CIN		0110011	0011	(Y=0, CIN=0)
//			sbc  : Z-X-Y-CIN		0110011	0011	(Y=0, CIN=CF)
//			neg  : -Z+X+Y+CIN-1		0110000 0001	(X=0, CIN=1)
//			not  : Z~^X				0111000	0100	(X=0, Y=48'hffffffffffff)
//			or   : Z|X				0111011	1100	(Y=48'hffffffffffff)
//			and  : Z&X				0110011	1100	(Y=0)
//			xor  : Z^X				0110011	0100	(Y=0)
// 		
// 		
// 		
// Dependencies: 	
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU(
	input wire [31:0] src0,
	input wire [31:0] src1,
	input wire clk,
	input wire [6:0] op,
	input wire [3:0] alu,
	input wire icf,
	output wire [31:0] dst,
	output wire ozf,
	output wire ocf
	);
	
	wire [14:0] dummy;
	
	DSP48E1 #(
		.A_INPUT("DIRECT"),
		.B_INPUT("DIRECT"),
		.USE_DPORT("FALSE"),
		.USE_MULT("NONE"),
		.AUTORESET_PATDET("NO_RESET"),
		.MASK(48'hffff00000000),
		.PATTERN(48'h000000000000),
		.SEL_MASK("MASK"),
		.SEL_PATTERN("PATTERN"),
		.USE_PATTERN_DETECT("PATDET"),
		.USE_SIMD("ONE48"),
		.ALUMODEREG(0),
		.INMODEREG(0),
		.OPMODEREG(0),
		.AREG(0),
		.BREG(0),
		.CREG(0),
		.DREG(1),
		.MREG(0),
		.PREG(0),
		.ADREG(0),
		.ACASCREG(0),
		.BCASCREG(0),
		.CARRYINREG(0),
		.CARRYINSELREG(0)
	)
	ALU_DSP (
		.CLK(clk),
		.ALUMODE(alu),
		.INMODE(5'b00000),
		.OPMODE(op),
		.A({16'h0000, src1[31:18]}),
		.B(src1[17:0]),
		.C({16'h0000, src0}),
		.D(25'h1ffffff),
		.P({dummy, ocf, dst}),
		.CARRYIN(icf),
		.ACIN(30'h00000000),
		.BCIN(18'h00000),
		.PCIN(48'h000000000000),
		.CARRYINSEL(1'b000),
		.CARRYCASCIN(1'b0),
		.MULTSIGNIN(1'b0),
		.CARRYOUT(),
		.ACOUT(),
		.BCOUT(),
		.PCOUT(),
		.PATTERNDETECT(ozf),
		.PATTERNBDETECT(),
		.CARRYCASCOUT(),
		.MULTSIGNOUT(),
		.OVERFLOW(),
		.UNDERFLOW(),
		.CEALUMODE(1'b0),
		.CEINMODE(1'b0),
		.CECTRL(1'b0),
		.CEA1(1'b0),
		.CEA2(1'b0),
		.CEB1(1'b0),
		.CEB2(1'b0),
		.CEC(1'b0),
		.CED(1'b0),
		.CEM(1'b0),
		.CEP(1'b0),
		.CEAD(1'b0),
		.CECARRYIN(1'b0),
		.RSTALUMODE(1'b0),
		.RSTINMODE(1'b0),
		.RSTCTRL(1'b0),
		.RSTA(1'b0),
		.RSTB(1'b0),
		.RSTC(1'b0),
		.RSTD(1'b0),
		.RSTM(1'b0),
		.RSTP(1'b0),
		.RSTALLCARRYIN(1'b0)
	);

endmodule
