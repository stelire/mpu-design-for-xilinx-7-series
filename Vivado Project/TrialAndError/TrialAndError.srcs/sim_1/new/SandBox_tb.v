`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/03/01 13:48:48
// Design Name: 
// Module Name: SandBox_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SandBox_tb;
	
	reg [7:0] src;
	wire [7:0] dst;
	
	SandBox uut(
		.src(src),
		.dst(dst)
	);
	
	integer i;
	
	initial begin
		src <= 8'h00;
		
		for(i=0; i<256; i=i+1) begin
			#10
			src <= i;
		end
		
		#30
		$finish;
	end
endmodule
