`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/21 14:21:45
// Design Name: 
// Module Name: MHB_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MHB_tb;

	reg [31:0] src;
	wire [31:0] dst;

	MostHBit uut (
		.src(src),
		.dst(dst)
	);
	
	integer i;
	
	initial begin
		for(i=0; i<32; i=i+1) begin
			#10
			src <= 32'hdeadbeef >> i;
		end
		#10
		$finish;
	end
	
endmodule
