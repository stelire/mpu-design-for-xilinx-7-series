`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/12 17:08:43
// Design Name: 
// Module Name: ATD_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ATD_tb;

	reg [31:0] src0;
	reg [31:0] src1;
	wire [31:0] dst0;
	wire [31:0] dst1;
	wire [31:0] dst2;
	wire [31:0] dst3;
	
	integer i;
	
	AssistToDivide uut (
		.src0(src0),
		.src1(src1),
		.dst0(dst0),
		.dst1(dst1),
		.dst2(dst2),
		.dst3(dst3)
	);
	
	initial begin
		src0 <= 32'h0a5a5a5a;
		src1 <= 32'h000000a6;
		#10
		src0 <= 32'h005a5a5a;
		src1 <= 32'h000000a6;
		#10
		src0 <= 32'h0000dead;
		src1 <= 32'h000000a6;
/*		
		for(i=0; i<33; i=i+1) begin
			#10
			src0 <= 32'hca5a5a5a >> i;
		end
*/
		#30
		$finish;
	end
	
endmodule
