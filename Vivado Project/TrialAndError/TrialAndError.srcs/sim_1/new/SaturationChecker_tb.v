`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/25 11:56:50
// Design Name: 
// Module Name: SaturationChecker_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SaturationChecker_tb;
	reg [31:0] src0;
	reg [31:0] src1;
	wire [31:0] dst0;
	wire [31:0] dst1;
	
	SaturationChecker uut(
		.src0(src0),
		.src1(src1),
		.dst0(dst0),
		.dst1(dst1)
	);
	
	initial begin
		#100
		src0 <= 32'h0a5a5a5a;
		src1 <= 32'h000000a6;
		
		#30 $finish;
	end
	
endmodule
