`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/27 10:57:28
// Design Name: 
// Module Name: MUX16_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUX16_tb;
	
	reg [15:0] in;
	reg [3:0] sel;
	wire out;
	
	MUX16 uut (
		.in(in),
		.sel(sel),
		.out(out)
	);
	
	integer i;
	
	initial begin
		in = 16'h1234;
		sel = 4'h0;
		
		for(i=0; i<16; i=i+1) begin
			#10
			sel <= i;
		end
		#30
		$finish;
	end
endmodule
