`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/15 14:41:07
// Design Name: 
// Module Name: MostHBit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MostHBit(
	input wire [31:0] src,
	output wire [31:0] dst
	);
	
	wire w31_26;
	wire w25_24;
	wire w25_21;
	wire w20_19;
	wire w20_16;
	wire w15_14;
	wire w15_11;
	wire w13_12;
	wire w10_9;
	wire w10_6;
	wire h10_7;
	wire h10_6;
	wire h5_4;
	wire h5_3;
	wire h5_2;
	wire h5_1;
	wire h5_0;
	
	assign w31_26 = ~|src[31:26];
	assign w25_24 = ~|src[25:24];
	assign w25_21 = ~|src[25:21];
	assign w20_19 = ~|src[20:19];
	assign w20_16 = ~|src[20:16];
	assign w15_14 = ~|src[15:14];
	assign w15_11 = ~|src[15:11];
	assign w13_12 = ~|src[13:12];
	assign w10_9 = ~|src[10:9];
	assign w10_6 = ~|src[10:6];
	assign h10_7 = ~|src[10:8] & src[7];
	assign h10_6 = ~|src[10:7] & src[6];
	assign h5_4 = ~src[5] & src[4];
	assign h5_3 = ~|src[5:4] & src[3];
	assign h5_2 = ~|src[5:3] & src[2];
	assign h5_1 = ~|src[5:2] & src[1];
	assign h5_0 = ~|src[5:1] & src[0];
	
	assign dst[31] = src[31];
	assign dst[30] = src[30] & ~src[31];
	assign dst[29] = src[29] & ~|src[31:30];
	assign dst[28] = src[28] & ~|src[31:29];
	assign dst[27] = src[27] & ~|src[31:28];
	assign dst[26] = src[26] & ~|src[31:27];
	assign dst[25] = src[25] & w31_26;
	assign dst[24] = src[24] & ~src[25] & w31_26;
	assign dst[23] = src[23] & ~|src[25:24] & w31_26;
	assign dst[22] = src[22] & ~|src[25:23] & w31_26;
	assign dst[21] = src[21] & ~|src[25:22] & w31_26;
	assign dst[20] = src[20] & ~|src[23:21]& w25_24 & w31_26;
	assign dst[19] = src[19] & ~src[20] & w25_21 & w31_26;
	assign dst[18] = src[18] & ~|src[20:19] & w25_21 & w31_26;
	assign dst[17] = src[17] & ~|src[20:18] & w25_21 & w31_26;
	assign dst[16] = src[16] & ~|src[18:17] & w20_19 & w25_21 & w31_26;
	assign dst[15] = src[15] & w20_16 & w25_21 & w31_26;
	assign dst[14] = src[14] & ~src[15] & w20_16 & w25_21 & w31_26;
	assign dst[13] = src[13] & ~|src[15:14] & w20_16 & w25_21 & w31_26;
	assign dst[12] = src[12] & ~src[13] & w15_14 & w20_16 & w25_21 & w31_26;
	assign dst[11] = src[11] & w13_12 & w15_14 & w20_16 & w25_21 & w31_26;
	assign dst[10] = src[10] & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[9] = src[9] & ~src[10] & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[8] = src[8] & w10_9 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[7] = h10_7 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[6] = h10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[5] = src[5] & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[4] = h5_4 & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[3] = h5_3 & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[2] = h5_2 & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[1] = h5_1 & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;
	assign dst[0] = h5_0 & w10_6 & w15_11 & w20_16 & w25_21 & w31_26;

/*
	LUT6 #(	.INIT(64'h0000000000000001))	LUT31_26	(.O(w31_26),.I5(src[31]),.I4(src[30]),.I3(src[29]),.I2(src[28]),.I1(src[27]),.I0(src[26]));
	LUT5 #(	.INIT(32'h00000001))			LUT25_21	(.O(w25_21),.I4(src[25]),.I3(src[24]),.I2(src[23]),.I1(src[22]),.I0(src[21]));
	LUT5 #(	.INIT(32'h00000001))			LUT20_16	(.O(w20_16),.I4(src[20]),.I3(src[19]),.I2(src[18]),.I1(src[17]),.I0(src[16]));
	LUT5 #(	.INIT(32'h00000001))			LUT15_11	(.O(w15_11),.I4(src[15]),.I3(src[14]),.I2(src[13]),.I1(src[12]),.I0(src[11]));
	LUT5 #(	.INIT(32'h00000001))			LUT10_6		(.O(w10_6),.I4(src[10]),.I3(src[9]),.I2(src[8]),.I1(src[7]),.I0(src[6]));
	LUT6 #(	.INIT(64'h0000000000000002))	LUT5_0h		(.O(h5_0),.I5(src[5]),.I4(src[4]),.I3(src[3]),.I2(src[2]),.I1(src[1]),.I0(src[0]));
	LUT4 #(	.INIT(16'h0001))				LUT20_17	(.O(w20_17),.I3(src[20]),.I2(src[19]),.I1(src[18]),.I0(src[17]));
	LUT4 #(	.INIT(16'h0002))				LUT15_12h	(.O(h15_12),.I3(src[15]),.I2(src[14]),.I1(src[13]),.I0(src[12]));
	LUT4 #(	.INIT(16'h0001))				LUT15_12	(.O(w15_12),.I3(src[15]),.I2(src[14]),.I1(src[13]),.I0(src[12]));
	LUT2 #(	.INIT(4'h1))					LUT10_9		(.O(w10_9),.I1(src[10]),.I0(src[9]));
	LUT4 #(	.INIT(16'h0002))				LUT10_7h	(.O(h10_7),.I3(src[10]),.I2(src[9]),.I1(src[8]),.I0(src[7]));
	LUT5 #(	.INIT(32'h00000002))			LUT10_6h	(.O(h10_6),.I4(src[10]),.I3(src[9]),.I2(src[8]),.I1(src[7]),.I0(src[6]));
	LUT2 #(	.INIT(4'h2))					LUT5_4h		(.O(h5_4),.I1(src[5]),.I0(src[4]));
	LUT3 #(	.INIT(8'h02))					LUT5_3h		(.O(h5_3),.I2(src[5]),.I1(src[4]),.I0(src[3]));
	LUT4 #(	.INIT(16'h0002))				LUT5_2h		(.O(h5_2),.I3(src[5]),.I2(src[4]),.I1(src[3]),.I0(src[2]));
	LUT5 #(	.INIT(32'h00000002))			LUT5_1h		(.O(h5_1),.I4(src[5]),.I3(src[4]),.I2(src[3]),.I1(src[2]),.I0(src[1]));
*/

/*	
	wire w31_26;
	wire w25_21;
	wire w16_12;
	wire w11_8;
	wire w7_5;
	wire w31_17;
	wire w16_3;
	wire w11_5;
	wire w16_8;
	wire w31_21;
	 	
	assign w31_26 = ~|src[31:26];
	assign w25_21 = ~|src[25:21];
	assign w16_12 = ~|src[16:12];
	assign w11_8 = ~|src[11:8];
	assign w7_5 = ~|src[7:5];
	assign w31_17 = w31_26 & w25_21 & ~|src[20:17];
	assign w16_3 = w16_12 & w11_8 & w7_5 & ~|src[4:3];
	assign w11_5 = w11_8 & w7_5;
	assign w16_8 = w16_12 & w11_8;
	assign w31_21 = w31_26 & w25_21;
	
	assign dst[31] = src[31];
	assign dst[30] = src[30] & ~src[31];
	assign dst[29] = src[29] & ~|src[31:30];
	assign dst[28] = src[28] & ~|src[31:29];
	assign dst[27] = src[27] & ~|src[31:28];
	assign dst[26] = src[26] & ~|src[31:27];
	assign dst[25] = src[25] & w31_26;
	assign dst[24] = src[24] & ~src[25] & w31_26;
	assign dst[23] = src[23] & ~|src[25:24] & w31_26;
	assign dst[22] = src[22] & ~|src[25:23] & w31_26;
	assign dst[21] = src[21] & ~|src[25:22] & w31_26;
	assign dst[20] = src[20] & w31_21;
	assign dst[19] = src[19] & ~src[20] & w31_21;
	assign dst[18] = src[18] & ~|src[20:19] & w31_21;
	assign dst[17] = src[17] & ~|src[20:18] & w31_21;
	assign dst[16] = src[16] & w31_17;
	assign dst[15] = src[15] & ~src[16] & w31_17;
	assign dst[14] = src[14] & ~|src[16:15] & w31_17;
	assign dst[13] = src[13] & ~|src[16:14] & w31_17;
	assign dst[12] = src[12] & ~|src[16:13] & w31_17;
	assign dst[11] = src[11] & w16_12 & w31_17;
	assign dst[10] = src[10] & ~src[11] & w16_12 & w31_17;
	assign dst[9] = src[9] & ~|src[11:10] & w16_12 & w31_17;
	assign dst[8] = src[8] & ~|src[11:9] & w16_12 & w31_17;
	assign dst[7] = src[7] & w16_8 & w31_17;
	assign dst[6] = src[6] & ~src[7] & w16_8 & w31_17;
	assign dst[5] = src[5] & ~|src[7:6] & w16_8 & w31_17;
	assign dst[4] = src[4] & w11_5 & w16_12 & w31_17;
	assign dst[3] = src[3] & ~src[4] & w11_5 & w16_12 & w31_17;
	assign dst[2] = src[2] & w16_3 & w31_17;
	assign dst[1] = src[1] & ~src[2] & w16_3 & w31_17;
	assign dst[0] = src[0] & ~|src[2:1] & w16_3 & w31_17;
*/	
endmodule
