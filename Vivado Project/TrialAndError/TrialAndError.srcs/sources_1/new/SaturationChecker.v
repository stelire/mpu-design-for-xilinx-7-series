`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/25 10:49:28
// Design Name: 
// Module Name: SaturationChecker
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SaturationChecker(
	input wire [31:0] src0,
	input wire [31:0] src1,
	output wire [31:0] dst0,
	output wire [31:0] dst1
	);
	
	wire [31:0] shift [31:0];
	wire [31:0] greater;
	wire [31:0] overflow;
	wire [31:0] pos;
	
//	assign pos = greater & ~overflow;

	generate
		genvar i;
		for(i=0; i<32; i=i+1) begin : Parallel
			assign shift[i] = src1 << i;
			assign pos[i] = (src0 >= shift[i]) & ~|(src1 >> (32 - i));
//			assign overflow[i] = |(src1 >> (32 - i));
//			assign greater[i] = (src0 >= shift[i]);
		end
	endgenerate
	
	assign dst0 =	(pos[31] == 1'b1) ?	shift[31] :
					(pos[30] == 1'b1) ?	shift[30] :
					(pos[29] == 1'b1) ?	shift[29] :
					(pos[28] == 1'b1) ?	shift[28] :
					(pos[27] == 1'b1) ?	shift[27] :
					(pos[26] == 1'b1) ?	shift[26] :
					(pos[25] == 1'b1) ?	shift[25] :
					(pos[24] == 1'b1) ?	shift[24] :
					(pos[23] == 1'b1) ?	shift[23] :
					(pos[22] == 1'b1) ?	shift[22] :
					(pos[21] == 1'b1) ?	shift[21] :
					(pos[20] == 1'b1) ?	shift[20] :
					(pos[19] == 1'b1) ?	shift[19] :
					(pos[18] == 1'b1) ?	shift[18] :
					(pos[17] == 1'b1) ?	shift[17] :
					(pos[16] == 1'b1) ?	shift[16] :
					(pos[15] == 1'b1) ?	shift[15] :
					(pos[14] == 1'b1) ?	shift[14] :
					(pos[13] == 1'b1) ?	shift[13] :
					(pos[12] == 1'b1) ?	shift[12] :
					(pos[11] == 1'b1) ?	shift[11] :
					(pos[10] == 1'b1) ?	shift[10] :
					(pos[9] == 1'b1) ?	shift[9] :
					(pos[8] == 1'b1) ?	shift[8] :
					(pos[7] == 1'b1) ?	shift[7] :
					(pos[6] == 1'b1) ?	shift[6] :
					(pos[5] == 1'b1) ?	shift[5] :
					(pos[4] == 1'b1) ?	shift[4] :
					(pos[3] == 1'b1) ?	shift[3] :
					(pos[2] == 1'b1) ?	shift[2] :
					(pos[1] == 1'b1) ?	shift[1] :
										shift[0];

/*	
	assign dst1[31] = pos[31];
	assign dst1[30] = pos[30] & ~pos[31];
	assign dst1[29] = pos[29] & ~|pos[31:30];
	assign dst1[28] = pos[28] & ~|pos[31:29];
	assign dst1[27] = pos[27] & ~|pos[31:28];
	assign dst1[26] = pos[26] & ~|pos[31:27];
	assign dst1[25] = pos[25] & ~|pos[31:26];
	assign dst1[24] = pos[24] & ~|pos[31:25];
	assign dst1[23] = pos[23] & ~|pos[31:24];
	assign dst1[22] = pos[22] & ~|pos[31:23];
	assign dst1[21] = pos[21] & ~|pos[31:22];
	assign dst1[20] = pos[20] & ~|pos[31:21];
	assign dst1[19] = pos[19] & ~|pos[31:20];
	assign dst1[18] = pos[18] & ~|pos[31:19];
	assign dst1[17] = pos[17] & ~|pos[31:18];
	assign dst1[16] = pos[16] & ~|pos[31:17];
	assign dst1[15] = pos[15] & ~|pos[31:16];
	assign dst1[14] = pos[14] & ~|pos[31:15];
	assign dst1[13] = pos[13] & ~|pos[31:14];
	assign dst1[12] = pos[12] & ~|pos[31:13];
	assign dst1[11] = pos[11] & ~|pos[31:12];
	assign dst1[10] = pos[10] & ~|pos[31:11];
	assign dst1[9] = pos[9] & ~|pos[31:10];
	assign dst1[8] = pos[8] & ~|pos[31:9];
	assign dst1[7] = pos[7] & ~|pos[31:8];
	assign dst1[6] = pos[6] & ~|pos[31:7];
	assign dst1[5] = pos[5] & ~|pos[31:6];
	assign dst1[4] = pos[4] & ~|pos[31:5];
	assign dst1[3] = pos[3] & ~|pos[31:4];
	assign dst1[2] = pos[2] & ~|pos[31:3];
	assign dst1[1] = pos[1] & ~|pos[31:2];
	assign dst1[0] = pos[0] & ~|pos[31:1];
*/	
	
	assign dst1 =	(pos[31] == 1'b1) ?	32'h80000000 :
					(pos[30] == 1'b1) ?	32'h40000000 :
					(pos[29] == 1'b1) ?	32'h20000000 :
					(pos[28] == 1'b1) ?	32'h10000000 :
					(pos[27] == 1'b1) ?	32'h08000000 :
					(pos[26] == 1'b1) ?	32'h04000000 :
					(pos[25] == 1'b1) ?	32'h02000000 :
					(pos[24] == 1'b1) ?	32'h01000000 :
					(pos[23] == 1'b1) ?	32'h00800000 :
					(pos[22] == 1'b1) ?	32'h00400000 :
					(pos[21] == 1'b1) ?	32'h00200000 :
					(pos[20] == 1'b1) ?	32'h00100000 :
					(pos[19] == 1'b1) ?	32'h00080000 :
					(pos[18] == 1'b1) ?	32'h00040000 :
					(pos[17] == 1'b1) ?	32'h00020000 :
					(pos[16] == 1'b1) ?	32'h00010000 :
					(pos[15] == 1'b1) ?	32'h00008000 :
					(pos[14] == 1'b1) ?	32'h00004000 :
					(pos[13] == 1'b1) ?	32'h00002000 :
					(pos[12] == 1'b1) ?	32'h00001000 :
					(pos[11] == 1'b1) ?	32'h00000800 :
					(pos[10] == 1'b1) ?	32'h00000400 :
					(pos[9] == 1'b1) ?	32'h00000200 :
					(pos[8] == 1'b1) ?	32'h00000100 :
					(pos[7] == 1'b1) ?	32'h00000080 :
					(pos[6] == 1'b1) ?	32'h00000040 :
					(pos[5] == 1'b1) ?	32'h00000020 :
					(pos[4] == 1'b1) ?	32'h00000010 :
					(pos[3] == 1'b1) ?	32'h00000008 :
					(pos[2] == 1'b1) ?	32'h00000004 :
					(pos[1] == 1'b1) ?	32'h00000002 :
					(pos[0] == 1'b1) ?	32'h00000001 :
										32'h00000000;

endmodule
