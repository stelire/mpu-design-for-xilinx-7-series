`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/27 15:00:34
// Design Name: 
// Module Name: SandBox
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SandBox(
	input wire [7:0] src,
	output wire [7:0] dst
	);

	assign dst[7] = src[7] | ~&src[6:0];	
	assign dst[6] = src[6] | ~&src[5:0];	
	assign dst[5] = src[5] | ~&src[4:0];	
	assign dst[4] = src[4] | ~&src[3:0];	
	assign dst[3] = src[3] | ~&src[2:0];	
	assign dst[2] = src[2] | ~&src[1:0];	
	assign dst[1] = src[1] | ~src[0];	
	assign dst[0] = src[0];	
		
endmodule
