`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/12 13:19:43
// Design Name: 
// Module Name: AssistToDevide
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AssistToDivide(
	input wire [31:0] src0,
	input wire [31:0] src1,
	output wire [31:0] dst0,
	output wire [31:0] dst1,
	output wire [31:0] dst2,
	output wire [31:0] dst3
	);
	
	wire [31:0] LeftPos0;
	wire [31:0] LeftPos1;
	wire [31:0] MaxPos0;
	wire [31:0] MaxPos1;
	wire [31:0] Src1Shift;
	

	assign dst0 = MaxPos0;
	assign dst1 = MaxPos1;
	assign dst2 = LeftPos0;
	assign dst3 = LeftPos1;


/*	
	assign MaskSrc0[31] = ~src0[31];
	assign MaskSrc0[30] = ~src0[30] & ~src0[31];
	assign MaskSrc0[29] = ~src0[29] & ~|src0[31:30];
	assign MaskSrc0[28] = ~src0[28] & ~|src0[31:29];
	assign MaskSrc0[27] = ~src0[27] & ~|src0[31:28];
	assign MaskSrc0[26] = ~src0[26] & ~|src0[31:27];
	assign MaskSrc0[25] = ~src0[25] & ~|src0[31:26];
	assign MaskSrc0[24] = ~src0[24] & ~|src0[31:25];
	assign MaskSrc0[23] = ~src0[23] & ~|src0[31:24];
	assign MaskSrc0[22] = ~src0[22] & ~|src0[31:23];
	assign MaskSrc0[21] = ~src0[21] & ~|src0[31:22];
	assign MaskSrc0[20] = ~src0[20] & ~|src0[31:21];
	assign MaskSrc0[19] = ~src0[19] & ~|src0[31:20];
	assign MaskSrc0[18] = ~src0[18] & ~|src0[31:19];
	assign MaskSrc0[17] = ~src0[17] & ~|src0[31:18];
	assign MaskSrc0[16] = ~src0[16] & ~|src0[31:17];
	assign MaskSrc0[15] = ~src0[15] & ~|src0[31:16];
	assign MaskSrc0[14] = ~src0[14] & ~|src0[31:15];
	assign MaskSrc0[13] = ~src0[13] & ~|src0[31:14];
	assign MaskSrc0[12] = ~src0[12] & ~|src0[31:13];
	assign MaskSrc0[11] = ~src0[11] & ~|src0[31:12];
	assign MaskSrc0[10] = ~src0[10] & ~|src0[31:11];
	assign MaskSrc0[9] = ~src0[9] & ~|src0[31:10];
	assign MaskSrc0[8] = ~src0[8] & ~|src0[31:9];
	assign MaskSrc0[7] = ~src0[7] & ~|src0[31:8];
	assign MaskSrc0[6] = ~src0[6] & ~|src0[31:7];
	assign MaskSrc0[5] = ~src0[5] & ~|src0[31:6];
	assign MaskSrc0[4] = ~src0[4] & ~|src0[31:5];
	assign MaskSrc0[3] = ~src0[3] & ~|src0[31:4];
	assign MaskSrc0[2] = ~src0[2] & ~|src0[31:3];
	assign MaskSrc0[1] = ~src0[1] & ~|src0[31:2];
	assign MaskSrc0[0] = ~src0[0] & ~|src0[31:1];


	assign Src1Shift =	(MaxPos0[31] == 1'b1) ?	(MaxPos1 >> 31) :
						(MaxPos0[30] == 1'b1) ?	(MaxPos1 >> 30) :
						(MaxPos0[29] == 1'b1) ?	(MaxPos1 >> 29) :
						(MaxPos0[28] == 1'b1) ?	(MaxPos1 >> 28) :
						(MaxPos0[27] == 1'b1) ?	(MaxPos1 >> 27) :
						(MaxPos0[26] == 1'b1) ?	(MaxPos1 >> 26) :
						(MaxPos0[25] == 1'b1) ?	(MaxPos1 >> 25) :
						(MaxPos0[24] == 1'b1) ?	(MaxPos1 >> 24) :
						(MaxPos0[23] == 1'b1) ?	(MaxPos1 >> 23) :
						(MaxPos0[22] == 1'b1) ?	(MaxPos1 >> 22) :
						(MaxPos0[21] == 1'b1) ?	(MaxPos1 >> 21) :
						(MaxPos0[20] == 1'b1) ?	(MaxPos1 >> 20) :
						(MaxPos0[19] == 1'b1) ?	(MaxPos1 >> 19) :
						(MaxPos0[18] == 1'b1) ?	(MaxPos1 >> 18) :
						(MaxPos0[17] == 1'b1) ?	(MaxPos1 >> 17) :
						(MaxPos0[16] == 1'b1) ?	(MaxPos1 >> 16) :
						(MaxPos0[15] == 1'b1) ?	(MaxPos1 >> 15) :
						(MaxPos0[14] == 1'b1) ?	(MaxPos1 >> 14) :
						(MaxPos0[13] == 1'b1) ?	(MaxPos1 >> 13) :
						(MaxPos0[12] == 1'b1) ?	(MaxPos1 >> 12) :
						(MaxPos0[11] == 1'b1) ?	(MaxPos1 >> 11) :
						(MaxPos0[10] == 1'b1) ?	(MaxPos1 >> 10) :
						(MaxPos0[9] == 1'b1) ?	(MaxPos1 >> 9) :
						(MaxPos0[8] == 1'b1) ?	(MaxPos1 >> 8) :
						(MaxPos0[7] == 1'b1) ?	(MaxPos1 >> 7) :
						(MaxPos0[6] == 1'b1) ?	(MaxPos1 >> 6) :
						(MaxPos0[5] == 1'b1) ?	(MaxPos1 >> 5) :
						(MaxPos0[4] == 1'b1) ?	(MaxPos1 >> 4) :
						(MaxPos0[3] == 1'b1) ?	(MaxPos1 >> 3) :
						(MaxPos0[2] == 1'b1) ?	(MaxPos1 >> 2) :
						(MaxPos0[1] == 1'b1) ?	(MaxPos1 >> 1) :
												MaxPos1;
*/										
	generate
		genvar i;
		for(i=0; i<32; i=i+1) begin : Parallel
			assign MaxPos0[i] = ({32'h00000000, src0} > ({32'h00000000, src1} << i));
/*			
			assign MaxPos0[i] = src0[31 - i] & ~|(src0 >> (32 - i));
			assign MaxPos1[i] = src1[31 - i] & ~|(src1 >> (32 - i));
			assign LeftPos0[i] = src0[i] & ~|(src0 >> (i + 1));
			assign LeftPos1[i] = src1[i] & ~|(src1 >> (i + 1));
*/			
		end
	endgenerate

endmodule
