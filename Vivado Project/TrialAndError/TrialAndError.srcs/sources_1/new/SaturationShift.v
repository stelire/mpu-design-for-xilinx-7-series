`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/18 16:33:24
// Design Name: 
// Module Name: SaturationShift
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SaturationShift(
	input wire [31:0] src0,
	input wire [31:0] src1,
	output wire [31:0] dst0,
	output wire [31:0] dst1
	);

	wire [31:0] LargeBit [31:0];
	wire [31:0] DiffBit [31:0];
	wire [31:0] LargeWord [31:0];
	wire [31:0] SftSrc1 [31:0];
	wire [31:0] FlgBitDrop;
	wire [31:0] SaturateWord;
	
	wire w31_26;
	wire w25_21;
	wire w16_12;
	wire w11_8;
	wire w7_5;
	wire w31_17;
	wire w16_3;
	wire w11_5;
	wire w16_8;
	wire w31_21;

	generate
		genvar i;
		for(i=0; i<32; i=i+1) begin : Parallel
			assign SftSrc1[i] = src1 << i;
			assign FlgBitDrop[i] = |(src1 >> (32 - i));
			assign DiffBit[i] = src0 | ~SftSrc1[i];
			assign LargeBit[i] = src0 & ~SftSrc1[i];
			
			assign LargeWord[i][31] = LargeBit[i][31] & DiffBit[i][31];
			assign LargeWord[i][30] = LargeBit[i][30] & &DiffBit[i][31:30];
			assign LargeWord[i][29] = LargeBit[i][29] & &DiffBit[i][31:29];
			assign LargeWord[i][28] = LargeBit[i][28] & &DiffBit[i][31:28];
			assign LargeWord[i][27] = LargeBit[i][27] & &DiffBit[i][31:27];
			assign LargeWord[i][26] = LargeBit[i][26] & &DiffBit[i][31:26];
			assign LargeWord[i][25] = LargeBit[i][25] & &DiffBit[i][31:25];
			assign LargeWord[i][24] = LargeBit[i][24] & &DiffBit[i][31:24];
			assign LargeWord[i][23] = LargeBit[i][23] & &DiffBit[i][31:23];
			assign LargeWord[i][22] = LargeBit[i][22] & &DiffBit[i][31:22];
			assign LargeWord[i][21] = LargeBit[i][21] & &DiffBit[i][31:21];
			assign LargeWord[i][20] = LargeBit[i][20] & &DiffBit[i][31:20];
			assign LargeWord[i][19] = LargeBit[i][19] & &DiffBit[i][31:19];
			assign LargeWord[i][18] = LargeBit[i][18] & &DiffBit[i][31:18];
			assign LargeWord[i][17] = LargeBit[i][17] & &DiffBit[i][31:17];
			assign LargeWord[i][16] = LargeBit[i][16] & &DiffBit[i][31:16];
			assign LargeWord[i][15] = LargeBit[i][15] & &DiffBit[i][31:15];
			assign LargeWord[i][14] = LargeBit[i][14] & &DiffBit[i][31:14];
			assign LargeWord[i][13] = LargeBit[i][13] & &DiffBit[i][31:13];
			assign LargeWord[i][12] = LargeBit[i][12] & &DiffBit[i][31:12];
			assign LargeWord[i][11] = LargeBit[i][11] & &DiffBit[i][31:11];
			assign LargeWord[i][10] = LargeBit[i][10] & &DiffBit[i][31:10];
			assign LargeWord[i][9] = LargeBit[i][9] & &DiffBit[i][31:9];
			assign LargeWord[i][8] = LargeBit[i][8] & &DiffBit[i][31:8];
			assign LargeWord[i][7] = LargeBit[i][7] & &DiffBit[i][31:7];
			assign LargeWord[i][6] = LargeBit[i][6] & &DiffBit[i][31:6];
			assign LargeWord[i][5] = LargeBit[i][5] & &DiffBit[i][31:5];
			assign LargeWord[i][4] = LargeBit[i][4] & &DiffBit[i][31:4];
			assign LargeWord[i][3] = LargeBit[i][3] & &DiffBit[i][31:3];
			assign LargeWord[i][2] = LargeBit[i][2] & &DiffBit[i][31:2];
			assign LargeWord[i][1] = LargeBit[i][1] & &DiffBit[i][31:1];
			assign LargeWord[i][0] = LargeBit[i][0] & &DiffBit[i][31:0];
			
			assign SaturateWord[i] = |LargeWord[i] & ~FlgBitDrop[i];
		end
	endgenerate

	assign w31_26 = ~|SaturateWord[31:26];
	assign w25_21 = ~|SaturateWord[25:21];
	assign w16_12 = ~|SaturateWord[16:12];
	assign w11_8 = ~|SaturateWord[11:8];
	assign w7_5 = ~|SaturateWord[7:5];
	assign w31_17 = w31_26 & w25_21 & ~|SaturateWord[20:17];
	assign w16_3 = w16_12 & w11_8 & w7_5 & ~|SaturateWord[4:3];
	assign w11_5 = w11_8 & w7_5;
	assign w16_8 = w16_12 & w11_8;
	assign w31_21 = w31_26 & w25_21;
	
	assign dst1[31] = SaturateWord[31];
	assign dst1[30] = SaturateWord[30] & ~SaturateWord[31];
	assign dst1[29] = SaturateWord[29] & ~|SaturateWord[31:30];
	assign dst1[28] = SaturateWord[28] & ~|SaturateWord[31:29];
	assign dst1[27] = SaturateWord[27] & ~|SaturateWord[31:28];
	assign dst1[26] = SaturateWord[26] & ~|SaturateWord[31:27];
	assign dst1[25] = SaturateWord[25] & w31_26;
	assign dst1[24] = SaturateWord[24] & ~SaturateWord[25] & w31_26;
	assign dst1[23] = SaturateWord[23] & ~|SaturateWord[25:24] & w31_26;
	assign dst1[22] = SaturateWord[22] & ~|SaturateWord[25:23] & w31_26;
	assign dst1[21] = SaturateWord[21] & ~|SaturateWord[25:22] & w31_26;
	assign dst1[20] = SaturateWord[20] & w31_21;
	assign dst1[19] = SaturateWord[19] & ~SaturateWord[20] & w31_21;
	assign dst1[18] = SaturateWord[18] & ~|SaturateWord[20:19] & w31_21;
	assign dst1[17] = SaturateWord[17] & ~|SaturateWord[20:18] & w31_21;
	assign dst1[16] = SaturateWord[16] & w31_17;
	assign dst1[15] = SaturateWord[15] & ~SaturateWord[16] & w31_17;
	assign dst1[14] = SaturateWord[14] & ~|SaturateWord[16:15] & w31_17;
	assign dst1[13] = SaturateWord[13] & ~|SaturateWord[16:14] & w31_17;
	assign dst1[12] = SaturateWord[12] & ~|SaturateWord[16:13] & w31_17;
	assign dst1[11] = SaturateWord[11] & w16_12 & w31_17;
	assign dst1[10] = SaturateWord[10] & ~SaturateWord[11] & w16_12 & w31_17;
	assign dst1[9] = SaturateWord[9] & ~|SaturateWord[11:10] & w16_12 & w31_17;
	assign dst1[8] = SaturateWord[8] & ~|SaturateWord[11:9] & w16_12 & w31_17;
	assign dst1[7] = SaturateWord[7] & w16_8 & w31_17;
	assign dst1[6] = SaturateWord[6] & ~SaturateWord[7] & w16_8 & w31_17;
	assign dst1[5] = SaturateWord[5] & ~|SaturateWord[7:6] & w16_8 & w31_17;
	assign dst1[4] = SaturateWord[4] & w11_5 & w16_12 & w31_17;
	assign dst1[3] = SaturateWord[3] & ~SaturateWord[4] & w11_5 & w16_12 & w31_17;
	assign dst1[2] = SaturateWord[2] & w16_3 & w31_17;
	assign dst1[1] = SaturateWord[1] & ~SaturateWord[2] & w16_3 & w31_17;
	assign dst1[0] = SaturateWord[0] & ~|SaturateWord[2:1] & w16_3 & w31_17;
	assign dst0 =	(dst1[31] == 1'b1) ?	SftSrc1[31] :
					(dst1[30] == 1'b1) ?	SftSrc1[30] :
					(dst1[29] == 1'b1) ?	SftSrc1[29] :
					(dst1[28] == 1'b1) ?	SftSrc1[28] :
					(dst1[27] == 1'b1) ?	SftSrc1[27] :
					(dst1[26] == 1'b1) ?	SftSrc1[26] :
					(dst1[25] == 1'b1) ?	SftSrc1[25] :
					(dst1[24] == 1'b1) ?	SftSrc1[24] :
					(dst1[23] == 1'b1) ?	SftSrc1[23] :
					(dst1[22] == 1'b1) ?	SftSrc1[22] :
					(dst1[21] == 1'b1) ?	SftSrc1[21] :
					(dst1[20] == 1'b1) ?	SftSrc1[20] :
					(dst1[19] == 1'b1) ?	SftSrc1[19] :
					(dst1[18] == 1'b1) ?	SftSrc1[18] :
					(dst1[17] == 1'b1) ?	SftSrc1[17] :
					(dst1[16] == 1'b1) ?	SftSrc1[16] :
					(dst1[15] == 1'b1) ?	SftSrc1[15] :
					(dst1[14] == 1'b1) ?	SftSrc1[14] :
					(dst1[13] == 1'b1) ?	SftSrc1[13] :
					(dst1[12] == 1'b1) ?	SftSrc1[12] :
					(dst1[11] == 1'b1) ?	SftSrc1[11] :
					(dst1[10] == 1'b1) ?	SftSrc1[10] :
					(dst1[9] == 1'b1) ?		SftSrc1[9] :
					(dst1[8] == 1'b1) ?		SftSrc1[8] :
					(dst1[7] == 1'b1) ?		SftSrc1[7] :
					(dst1[6] == 1'b1) ?		SftSrc1[6] :
					(dst1[5] == 1'b1) ?		SftSrc1[5] :
					(dst1[4] == 1'b1) ?		SftSrc1[4] :
					(dst1[3] == 1'b1) ?		SftSrc1[3] :
					(dst1[2] == 1'b1) ?		SftSrc1[2] :
					(dst1[1] == 1'b1) ?		SftSrc1[1] :
					(dst1[0] == 1'b1) ?		SftSrc1[0] :
											32'h00000000;

endmodule
