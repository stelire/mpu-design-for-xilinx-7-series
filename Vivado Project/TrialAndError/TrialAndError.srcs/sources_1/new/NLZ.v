`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/18 10:45:18
// Design Name: 
// Module Name: NLZ
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NLZ(
	input wire [31:0] src,
	output wire [5:0] dst
	);
	
	assign dst[5] = ~|src[31:0];
	assign dst[4] = ~|src[31:16] & |src[15:0];
	assign dst[3] = ~|src[31:24] & |src[23:16] | ~|src[31:8] & |src[7:0];
	assign dst[2] = ~|src[31:28] & |src[27:24] | ~|src[31:20] & |src[19:16] | ~|src[31:12] & |src[11:8] | ~|src[31:4] & |src[3:0];
	assign dst[1] = ~|src[31:30] & |src[29:28] | ~|src[31:26] & |src[25:24] | ~|src[31:22] & |src[21:20] | ~|src[31:18] & |src[17:16] |
					~|src[31:14] & |src[13:12] | ~|src[31:10] & |src[9:8] | ~|src[31:6] & |src[5:4] | ~|src[31:2] & |src[1:0];
	assign dst[0] = ~src[31] & src[30] | ~|src[31:29] & src[28] | ~|src[31:27] & src[26] | ~|src[31:25] & src[24] |
					~|src[31:23] & src[22] | ~|src[31:21] & src[20] | ~|src[31:19] & src[18] | ~|src[31:17] & src[16] | 
					~|src[31:15] & src[14] | ~|src[31:13] & src[12] | ~|src[31:11] & src[10] | ~|src[31:9] & src[8] |
					~|src[31:7] & src[6] | ~|src[31:5] & src[4] | ~|src[31:3] & src[2] | ~|src[31:1] & src[0];
	
endmodule
