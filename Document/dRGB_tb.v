`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:		Nippon Seiki co.,ltd.
// Engineer:	S.Nakamura
// 
// Create Date: 2019/11/16 13:15:00
// Design Name: dRGB Signal Test Bench
// Module Name: dRGB_tb
// Project Name: 
// Target Devices: Artix 7
// Tool Versions: Vivado 2016.3
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 1.00 - Completion
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dRGB_tb;
	parameter integer C_HS = 32;
	parameter integer C_HBP = 68;
	parameter integer C_HDISP = 320;
	parameter integer C_HFP = 28;
	parameter integer C_VS = 4;
	parameter integer C_VBP = 18;
	parameter integer C_VDISP = 180;
	parameter integer C_VFP = 64;
	parameter C_HPOLARITY = 1'b1;	// 1'b0...pos  1'b1...neg
	parameter C_VPOLARITY = 1'b1;	// 1'b0...pos  1'b1...neg

	parameter integer C_INITIAL_TIME = 100;
	parameter integer C_PCLK_FREQ = 100;	// MHz
	parameter integer C_PCLK_CYCLE = 1000 / C_PCLK_FREQ;	// ns
	
	reg pclk;
	reg hsync;
	reg vsync;
	reg de;

	wire ode;
	
	DE_gen uut (
		.pclk(pclk),
		.hsync(hsync ^ C_HPOLARITY),
		.vsync(vsync ^ C_VPOLARITY),
		.de(ode)
	);
	
	/********************************************************/
	/* dRGB Signal Test Bench								*/
	/*			S Nakamura	2019/01/15						*/
	/*		pclk											*/
	/*		hsync											*/
	/*		vsync											*/
	/*		de												*/
	/********************************************************/
	
	
	initial begin
		pclk <= 1'b1;
		forever begin
			#(C_PCLK_CYCLE / 2) pclk <= ~pclk;
		end
	end

	initial begin
		hsync = 1'b0;
		#C_INITIAL_TIME;
		forever begin
			hsync = 1'b1;
			#(C_HS * C_PCLK_CYCLE);
			hsync = 1'b0;
			#((C_HBP + C_HDISP + C_HFP) * C_PCLK_CYCLE);
		end
	end
		
	initial begin
		vsync = 1'b0;
		#C_INITIAL_TIME;
		forever begin
			vsync = 1'b1;
			#(C_VS * (C_HS + C_HBP + C_HDISP + C_HFP) * C_PCLK_CYCLE);
			vsync = 1'b0;
			#((C_VBP + C_VDISP + C_VFP) * (C_HS + C_HBP + C_HDISP + C_HFP) * C_PCLK_CYCLE);
		end
	end
		
	initial begin
		de = 1'b0;
		#C_INITIAL_TIME;
		forever begin
			#((C_VS + C_VBP) * (C_HS + C_HBP + C_HDISP + C_HFP) * C_PCLK_CYCLE);
			repeat(C_VDISP) begin
				#((C_HS + C_HBP) * C_PCLK_CYCLE);
				de = 1'b1;
				#(C_HDISP * C_PCLK_CYCLE);
				de = 1'b0;
				#(C_HFP * C_PCLK_CYCLE);
			end
			#(C_VFP * (C_HS + C_HBP + C_HDISP + C_HFP) * C_PCLK_CYCLE);
		end
	end
	
endmodule
