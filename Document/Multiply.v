`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/02/05 17:36:41
// Design Name: 
// Module Name: DSP_ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DSP_ALU_base(
	input wire [31:0] src0,
	input wire [31:0] src1,
	input wire clk,
	input wire [6:0] op,
	input wire [3:0] alu,
	input wire [4:0] in,
	input wire icf,
	output wire [63:0] dst,
	output wire ozf,
	output wire ocf
	);

//	assign dst = src0 * src1;

	
	wire [29:0] ac_h;
	wire [29:0] ac_l;
	wire [47:0] pc_h;
	wire [47:0] pc_l;
	wire [47:0] pc_hl;
	wire [30:0] dummy_h0;
	wire [16:0] dummy_h1;
	wire [30:0] dummy_l0;
	wire dummy_l1;
	wire zf_h0;
	wire zf_h1;
	wire zf_l0;
	wire zf_l1;
	
	assign ozf = zf_h0 & zf_h1 & zf_l0 & zf_l1;
	
	DSP48E1 #(
		// Feature Control Attributes: Data Path Selection
		.A_INPUT("DIRECT"), // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
		.B_INPUT("DIRECT"), // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
		.USE_DPORT("FALSE"), // Select D port usage (TRUE or FALSE)
		.USE_MULT("MULTIPLY"), // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
		// Pattern Detector Attributes: Pattern Detection Configuration
		.AUTORESET_PATDET("NO_RESET"), // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
		.MASK(48'hfffe00000000), // 48-bit mask value for pattern detect (1=ignore)
		.PATTERN(48'h000000000000), // 48-bit pattern match for pattern detect
		.SEL_MASK("MASK"), // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
		.SEL_PATTERN("PATTERN"), // Select pattern value ("PATTERN" or "C")
		.USE_PATTERN_DETECT("PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
		// Register Control Attributes: Pipeline Register Configuration
		.ACASCREG(0), // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
		.ADREG(0), // Number of pipeline stages for pre-adder (0 or 1)
		.ALUMODEREG(0), // Number of pipeline stages for ALUMODE (0 or 1)
		.AREG(0), // Number of pipeline stages for A (0, 1 or 2)
		.BCASCREG(0), // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
		.BREG(0), // Number of pipeline stages for B (0, 1 or 2)
		.CARRYINREG(0), // Number of pipeline stages for CARRYIN (0 or 1)
		.CARRYINSELREG(0), // Number of pipeline stages for CARRYINSEL (0 or 1)
		.CREG(0), // Number of pipeline stages for C (0 or 1)
		.DREG(0), // Number of pipeline stages for D (0 or 1)
		.INMODEREG(0), // Number of pipeline stages for INMODE (0 or 1)
		.MREG(0), // Number of multiplier pipeline stages (0 or 1)
		.OPMODEREG(0), // Number of pipeline stages for OPMODE (0 or 1)
		.PREG(0), // Number of pipeline stages for P (0 or 1)
		.USE_SIMD("ONE48") // SIMD selection ("ONE48", "TWO24", "FOUR12")
	)
	DSP_Multiply_h0 (
		// Cascade: 30-bit (each) output: Cascade Ports
		.ACOUT(ac_h), // 30-bit output: A port cascade output
		.BCOUT(), // 18-bit output: B port cascade output
		.CARRYCASCOUT(), // 1-bit output: Cascade carry output
		.MULTSIGNOUT(), // 1-bit output: Multiplier sign cascade output
		.PCOUT(pc_h), // 48-bit output: Cascade output
		// Control: 1-bit (each) output: Control Inputs/Status Bits
		.OVERFLOW(), // 1-bit output: Overflow in add/acc output
		.PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
		.PATTERNDETECT(zf_h0), // 1-bit output: Pattern detect output
		.UNDERFLOW(), // 1-bit output: Underflow in add/acc output
		// Data: 4-bit (each) output: Data Ports
		.CARRYOUT(), // 4-bit output: Carry output
		.P({dummy_h0,dst[32:16]}), // 48-bit output: Primary data output
		// Cascade: 30-bit (each) input: Cascade Ports
		.ACIN(30'h00000000), // 30-bit input: A cascade data input
		.BCIN(18'h00000), // 18-bit input: B cascade input
		.CARRYCASCIN(1'b0), // 1-bit input: Cascade carry input
		.MULTSIGNIN(1'b0), // 1-bit input: Multiplier sign input
		.PCIN(48'h000000000000), // 48-bit input: P cascade input
		// Control: 4-bit (each) input: Control Inputs/Status Bits
		.ALUMODE(4'b0000), // 4-bit input: ALU control input
		.CARRYINSEL(3'b000), // 3-bit input: Carry select input
		.CEINMODE(1'b1), // 1-bit input: Clock enable input for INMODEREG
		.CLK(clk), // 1-bit input: Clock input
		.INMODE(5'b00000), // 5-bit input: INMODE control input
		.OPMODE(7'b0110101), // 7-bit input: Operation mode input
		.RSTINMODE(1'b0), // 1-bit input: Reset input for INMODEREG
		// Data: 30-bit (each) input: Data Ports
		.A({14'h0000, src0[31:16]}), // 30-bit input: A data input
		.B({1'h0, src1[16:0]}), // 18-bit input: B data input
		.C(pc_hl), // 48-bit input: C data input
		.CARRYIN(1'b0), // 1-bit input: Carry input signal
		.D(25'h0000000), // 25-bit input: D data input
		// Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
		.CEA1(1'b0), // 1-bit input: Clock enable input for 1st stage AREG
		.CEA2(1'b0), // 1-bit input: Clock enable input for 2nd stage AREG
		.CEAD(1'b0), // 1-bit input: Clock enable input for ADREG
		.CEALUMODE(1'b1), // 1-bit input: Clock enable input for ALUMODERE
		.CEB1(1'b0), // 1-bit input: Clock enable input for 1st stage BREG
		.CEB2(1'b0), // 1-bit input: Clock enable input for 2nd stage BREG
		.CEC(1'b1), // 1-bit input: Clock enable input for CREG
		.CECARRYIN(1'b0), // 1-bit input: Clock enable input for CARRYINREG
		.CECTRL(1'b1), // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
		.CED(1'b0), // 1-bit input: Clock enable input for DREG
		.CEM(1'b1), // 1-bit input: Clock enable input for MREG
		.CEP(1'b1), // 1-bit input: Clock enable input for PREG
		.RSTA(1'b0), // 1-bit input: Reset input for AREG
		.RSTALLCARRYIN(1'b0), // 1-bit input: Reset input for CARRYINREG
		.RSTALUMODE(1'b0), // 1-bit input: Reset input for ALUMODEREG
		.RSTB(1'b0), // 1-bit input: Reset input for BREG
		.RSTC(1'b0), // 1-bit input: Reset input for CREG
		.RSTCTRL(1'b0), // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
		.RSTD(1'b0), // 1-bit input: Reset input for DREG and ADREG
		.RSTM(1'b0), // 1-bit input: Reset input for MREG
		.RSTP(1'b0) // 1-bit input: Reset input for PREG
	);

	DSP48E1 #(
		// Feature Control Attributes: Data Path Selection
		.A_INPUT("CASCADE"), // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
		.B_INPUT("DIRECT"), // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
		.USE_DPORT("FALSE"), // Select D port usage (TRUE or FALSE)
		.USE_MULT("MULTIPLY"), // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
		// Pattern Detector Attributes: Pattern Detection Configuration
		.AUTORESET_PATDET("NO_RESET"), // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
		.MASK(48'hffff80000000), // 48-bit mask value for pattern detect (1=ignore)
		.PATTERN(48'h000000000000), // 48-bit pattern match for pattern detect
		.SEL_MASK("MASK"), // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
		.SEL_PATTERN("PATTERN"), // Select pattern value ("PATTERN" or "C")
		.USE_PATTERN_DETECT("PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
		// Register Control Attributes: Pipeline Register Configuration
		.ACASCREG(0), // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
		.ADREG(0), // Number of pipeline stages for pre-adder (0 or 1)
		.ALUMODEREG(0), // Number of pipeline stages for ALUMODE (0 or 1)
		.AREG(0), // Number of pipeline stages for A (0, 1 or 2)
		.BCASCREG(0), // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
		.BREG(0), // Number of pipeline stages for B (0, 1 or 2)
		.CARRYINREG(0), // Number of pipeline stages for CARRYIN (0 or 1)
		.CARRYINSELREG(0), // Number of pipeline stages for CARRYINSEL (0 or 1)
		.CREG(0), // Number of pipeline stages for C (0 or 1)
		.DREG(0), // Number of pipeline stages for D (0 or 1)
		.INMODEREG(0), // Number of pipeline stages for INMODE (0 or 1)
		.MREG(0), // Number of multiplier pipeline stages (0 or 1)
		.OPMODEREG(0), // Number of pipeline stages for OPMODE (0 or 1)
		.PREG(0), // Number of pipeline stages for P (0 or 1)
		.USE_SIMD("ONE48") // SIMD selection ("ONE48", "TWO24", "FOUR12")
	)
	DSP_Multiply_h1 (
		// Cascade: 30-bit (each) output: Cascade Ports
		.ACOUT(), // 30-bit output: A port cascade output
		.BCOUT(), // 18-bit output: B port cascade output
		.CARRYCASCOUT(), // 1-bit output: Cascade carry output
		.MULTSIGNOUT(), // 1-bit output: Multiplier sign cascade output
		.PCOUT(), // 48-bit output: Cascade output
		// Control: 1-bit (each) output: Control Inputs/Status Bits
		.OVERFLOW(), // 1-bit output: Overflow in add/acc output
		.PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
		.PATTERNDETECT(zf_h1), // 1-bit output: Pattern detect output
		.UNDERFLOW(), // 1-bit output: Underflow in add/acc output
		// Data: 4-bit (each) output: Data Ports
		.CARRYOUT(), // 4-bit output: Carry output
		.P({dummy_h1, dst[63:33]}), // 48-bit output: Primary data output
		// Cascade: 30-bit (each) input: Cascade Ports
		.ACIN(ac_h), // 30-bit input: A cascade data input
		.BCIN(18'h00000), // 18-bit input: B cascade input
		.CARRYCASCIN(1'b0), // 1-bit input: Cascade carry input
		.MULTSIGNIN(1'b0), // 1-bit input: Multiplier sign input
		.PCIN(pc_h), // 48-bit input: P cascade input
		// Control: 4-bit (each) input: Control Inputs/Status Bits
		.ALUMODE(4'b0000), // 4-bit input: ALU control input
		.CARRYINSEL(3'b000), // 3-bit input: Carry select input
		.CEINMODE(1'b1), // 1-bit input: Clock enable input for INMODEREG
		.CLK(clk), // 1-bit input: Clock input
		.INMODE(5'b00000), // 5-bit input: INMODE control input
		.OPMODE(7'b1010101), // 7-bit input: Operation mode input
		.RSTINMODE(1'b0), // 1-bit input: Reset input for INMODEREG
		// Data: 30-bit (each) input: Data Ports
		.A(30'h00000000), // 30-bit input: A data input
		.B({3'h0, src1[31:17]}), // 18-bit input: B data input
		.C(48'h000000000000), // 48-bit input: C data input
		.CARRYIN(1'b0), // 1-bit input: Carry input signal
		.D(25'h0000000), // 25-bit input: D data input
		// Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
		.CEA1(1'b0), // 1-bit input: Clock enable input for 1st stage AREG
		.CEA2(1'b0), // 1-bit input: Clock enable input for 2nd stage AREG
		.CEAD(1'b0), // 1-bit input: Clock enable input for ADREG
		.CEALUMODE(1'b1), // 1-bit input: Clock enable input for ALUMODERE
		.CEB1(1'b0), // 1-bit input: Clock enable input for 1st stage BREG
		.CEB2(1'b0), // 1-bit input: Clock enable input for 2nd stage BREG
		.CEC(1'b1), // 1-bit input: Clock enable input for CREG
		.CECARRYIN(1'b1), // 1-bit input: Clock enable input for CARRYINREG
		.CECTRL(1'b1), // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
		.CED(1'b0), // 1-bit input: Clock enable input for DREG
		.CEM(1'b1), // 1-bit input: Clock enable input for MREG
		.CEP(1'b1), // 1-bit input: Clock enable input for PREG
		.RSTA(1'b0), // 1-bit input: Reset input for AREG
		.RSTALLCARRYIN(1'b0), // 1-bit input: Reset input for CARRYINREG
		.RSTALUMODE(1'b0), // 1-bit input: Reset input for ALUMODEREG
		.RSTB(1'b0), // 1-bit input: Reset input for BREG
		.RSTC(1'b0), // 1-bit input: Reset input for CREG
		.RSTCTRL(1'b0), // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
		.RSTD(1'b0), // 1-bit input: Reset input for DREG and ADREG
		.RSTM(1'b0), // 1-bit input: Reset input for MREG
		.RSTP(1'b0) // 1-bit input: Reset input for PREG
	);

	DSP48E1 #(
		// Feature Control Attributes: Data Path Selection
		.A_INPUT("DIRECT"), // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
		.B_INPUT("DIRECT"), // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
		.USE_DPORT("FALSE"), // Select D port usage (TRUE or FALSE)
		.USE_MULT("MULTIPLY"), // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
		// Pattern Detector Attributes: Pattern Detection Configuration
		.AUTORESET_PATDET("NO_RESET"), // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
		.MASK(48'hfffe00000000), // 48-bit mask value for pattern detect (1=ignore)
		.PATTERN(48'h000000000000), // 48-bit pattern match for pattern detect
		.SEL_MASK("MASK"), // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
		.SEL_PATTERN("PATTERN"), // Select pattern value ("PATTERN" or "C")
		.USE_PATTERN_DETECT("PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
		// Register Control Attributes: Pipeline Register Configuration
		.ACASCREG(0), // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
		.ADREG(0), // Number of pipeline stages for pre-adder (0 or 1)
		.ALUMODEREG(0), // Number of pipeline stages for ALUMODE (0 or 1)
		.AREG(0), // Number of pipeline stages for A (0, 1 or 2)
		.BCASCREG(0), // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
		.BREG(0), // Number of pipeline stages for B (0, 1 or 2)
		.CARRYINREG(0), // Number of pipeline stages for CARRYIN (0 or 1)
		.CARRYINSELREG(0), // Number of pipeline stages for CARRYINSEL (0 or 1)
		.CREG(0), // Number of pipeline stages for C (0 or 1)
		.DREG(0), // Number of pipeline stages for D (0 or 1)
		.INMODEREG(0), // Number of pipeline stages for INMODE (0 or 1)
		.MREG(0), // Number of multiplier pipeline stages (0 or 1)
		.OPMODEREG(0), // Number of pipeline stages for OPMODE (0 or 1)
		.PREG(0), // Number of pipeline stages for P (0 or 1)
		.USE_SIMD("ONE48") // SIMD selection ("ONE48", "TWO24", "FOUR12")
	)
	DSP_Multiply_l0 (
		// Cascade: 30-bit (each) output: Cascade Ports
		.ACOUT(ac_l), // 30-bit output: A port cascade output
		.BCOUT(), // 18-bit output: B port cascade output
		.CARRYCASCOUT(), // 1-bit output: Cascade carry output
		.MULTSIGNOUT(), // 1-bit output: Multiplier sign cascade output
		.PCOUT(pc_l), // 48-bit output: Cascade output
		// Control: 1-bit (each) output: Control Inputs/Status Bits
		.OVERFLOW(), // 1-bit output: Overflow in add/acc output
		.PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
		.PATTERNDETECT(zf_l0), // 1-bit output: Pattern detect output
		.UNDERFLOW(), // 1-bit output: Underflow in add/acc output
		// Data: 4-bit (each) output: Data Ports
		.CARRYOUT(), // 4-bit output: Carry output
		.P({dummy_l0, pc_hl[0], dst[15:0]}), // 48-bit output: Primary data output
		// Cascade: 30-bit (each) input: Cascade Ports
		.ACIN(30'h00000000), // 30-bit input: A cascade data input
		.BCIN(18'h00000), // 18-bit input: B cascade input
		.CARRYCASCIN(1'b0), // 1-bit input: Cascade carry input
		.MULTSIGNIN(1'b0), // 1-bit input: Multiplier sign input
		.PCIN(48'h000000000000), // 48-bit input: P cascade input
		// Control: 4-bit (each) input: Control Inputs/Status Bits
		.ALUMODE(4'b0000), // 4-bit input: ALU control input
		.CARRYINSEL(3'b000), // 3-bit input: Carry select input
		.CEINMODE(1'b1), // 1-bit input: Clock enable input for INMODEREG
		.CLK(clk), // 1-bit input: Clock input
		.INMODE(5'b00000), // 5-bit input: INMODE control input
		.OPMODE(7'b0000101), // 7-bit input: Operation mode input
		.RSTINMODE(1'b0), // 1-bit input: Reset input for INMODEREG
		// Data: 30-bit (each) input: Data Ports
		.A({14'h0000, src0[15:0]}), // 30-bit input: A data input
		.B({1'h0, src1[16:0]}), // 18-bit input: B data input
		.C(48'h000000000000), // 48-bit input: C data input
		.CARRYIN(1'b0), // 1-bit input: Carry input signal
		.D(25'h0000000), // 25-bit input: D data input
		// Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
		.CEA1(1'b0), // 1-bit input: Clock enable input for 1st stage AREG
		.CEA2(1'b0), // 1-bit input: Clock enable input for 2nd stage AREG
		.CEAD(1'b0), // 1-bit input: Clock enable input for ADREG
		.CEALUMODE(1'b1), // 1-bit input: Clock enable input for ALUMODERE
		.CEB1(1'b0), // 1-bit input: Clock enable input for 1st stage BREG
		.CEB2(1'b0), // 1-bit input: Clock enable input for 2nd stage BREG
		.CEC(1'b1), // 1-bit input: Clock enable input for CREG
		.CECARRYIN(1'b0), // 1-bit input: Clock enable input for CARRYINREG
		.CECTRL(1'b1), // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
		.CED(1'b0), // 1-bit input: Clock enable input for DREG
		.CEM(1'b1), // 1-bit input: Clock enable input for MREG
		.CEP(1'b1), // 1-bit input: Clock enable input for PREG
		.RSTA(1'b0), // 1-bit input: Reset input for AREG
		.RSTALLCARRYIN(1'b0), // 1-bit input: Reset input for CARRYINREG
		.RSTALUMODE(1'b0), // 1-bit input: Reset input for ALUMODEREG
		.RSTB(1'b0), // 1-bit input: Reset input for BREG
		.RSTC(1'b0), // 1-bit input: Reset input for CREG
		.RSTCTRL(1'b0), // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
		.RSTD(1'b0), // 1-bit input: Reset input for DREG and ADREG
		.RSTM(1'b0), // 1-bit input: Reset input for MREG
		.RSTP(1'b0) // 1-bit input: Reset input for PREG
	);

	DSP48E1 #(
		// Feature Control Attributes: Data Path Selection
		.A_INPUT("CASCADE"), // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
		.B_INPUT("DIRECT"), // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
		.USE_DPORT("FALSE"), // Select D port usage (TRUE or FALSE)
		.USE_MULT("MULTIPLY"), // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
		// Pattern Detector Attributes: Pattern Detection Configuration
		.AUTORESET_PATDET("NO_RESET"), // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
		.MASK(48'hffff80000000), // 48-bit mask value for pattern detect (1=ignore)
		.PATTERN(48'h000000000000), // 48-bit pattern match for pattern detect
		.SEL_MASK("MASK"), // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
		.SEL_PATTERN("PATTERN"), // Select pattern value ("PATTERN" or "C")
		.USE_PATTERN_DETECT("PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
		// Register Control Attributes: Pipeline Register Configuration
		.ACASCREG(0), // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
		.ADREG(0), // Number of pipeline stages for pre-adder (0 or 1)
		.ALUMODEREG(0), // Number of pipeline stages for ALUMODE (0 or 1)
		.AREG(0), // Number of pipeline stages for A (0, 1 or 2)
		.BCASCREG(0), // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
		.BREG(0), // Number of pipeline stages for B (0, 1 or 2)
		.CARRYINREG(0), // Number of pipeline stages for CARRYIN (0 or 1)
		.CARRYINSELREG(0), // Number of pipeline stages for CARRYINSEL (0 or 1)
		.CREG(0), // Number of pipeline stages for C (0 or 1)
		.DREG(0), // Number of pipeline stages for D (0 or 1)
		.INMODEREG(0), // Number of pipeline stages for INMODE (0 or 1)
		.MREG(0), // Number of multiplier pipeline stages (0 or 1)
		.OPMODEREG(0), // Number of pipeline stages for OPMODE (0 or 1)
		.PREG(0), // Number of pipeline stages for P (0 or 1)
		.USE_SIMD("ONE48") // SIMD selection ("ONE48", "TWO24", "FOUR12")
	)
	DSP_Multiply_l1 (
		// Cascade: 30-bit (each) output: Cascade Ports
		.ACOUT(), // 30-bit output: A port cascade output
		.BCOUT(), // 18-bit output: B port cascade output
		.CARRYCASCOUT(), // 1-bit output: Cascade carry output
		.MULTSIGNOUT(), // 1-bit output: Multiplier sign cascade output
		.PCOUT(), // 48-bit output: Cascade output
		// Control: 1-bit (each) output: Control Inputs/Status Bits
		.OVERFLOW(), // 1-bit output: Overflow in add/acc output
		.PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
		.PATTERNDETECT(zf_l1), // 1-bit output: Pattern detect output
		.UNDERFLOW(), // 1-bit output: Underflow in add/acc output
		// Data: 4-bit (each) output: Data Ports
		.CARRYOUT(), // 4-bit output: Carry output
		.P({dummy_l1, pc_hl[47:1]}), // 48-bit output: Primary data output
		// Cascade: 30-bit (each) input: Cascade Ports
		.ACIN(ac_l), // 30-bit input: A cascade data input
		.BCIN(18'h00000), // 18-bit input: B cascade input
		.CARRYCASCIN(1'b0), // 1-bit input: Cascade carry input
		.MULTSIGNIN(1'b0), // 1-bit input: Multiplier sign input
		.PCIN(pc_l), // 48-bit input: P cascade input
		// Control: 4-bit (each) input: Control Inputs/Status Bits
		.ALUMODE(4'b0000), // 4-bit input: ALU control input
		.CARRYINSEL(3'b000), // 3-bit input: Carry select input
		.CEINMODE(1'b1), // 1-bit input: Clock enable input for INMODEREG
		.CLK(clk), // 1-bit input: Clock input
		.INMODE(5'b00000), // 5-bit input: INMODE control input
		.OPMODE(7'b1010101), // 7-bit input: Operation mode input
		.RSTINMODE(1'b0), // 1-bit input: Reset input for INMODEREG
		// Data: 30-bit (each) input: Data Ports
		.A(30'h00000000), // 30-bit input: A data input
		.B({3'h0, src1[31:17]}), // 18-bit input: B data input
		.C(48'h000000000000), // 48-bit input: C data input
		.CARRYIN(1'b0), // 1-bit input: Carry input signal
		.D(25'h0000000), // 25-bit input: D data input
		// Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
		.CEA1(1'b0), // 1-bit input: Clock enable input for 1st stage AREG
		.CEA2(1'b0), // 1-bit input: Clock enable input for 2nd stage AREG
		.CEAD(1'b0), // 1-bit input: Clock enable input for ADREG
		.CEALUMODE(1'b1), // 1-bit input: Clock enable input for ALUMODERE
		.CEB1(1'b0), // 1-bit input: Clock enable input for 1st stage BREG
		.CEB2(1'b0), // 1-bit input: Clock enable input for 2nd stage BREG
		.CEC(1'b1), // 1-bit input: Clock enable input for CREG
		.CECARRYIN(1'b1), // 1-bit input: Clock enable input for CARRYINREG
		.CECTRL(1'b1), // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
		.CED(1'b0), // 1-bit input: Clock enable input for DREG
		.CEM(1'b1), // 1-bit input: Clock enable input for MREG
		.CEP(1'b1), // 1-bit input: Clock enable input for PREG
		.RSTA(1'b0), // 1-bit input: Reset input for AREG
		.RSTALLCARRYIN(1'b0), // 1-bit input: Reset input for CARRYINREG
		.RSTALUMODE(1'b0), // 1-bit input: Reset input for ALUMODEREG
		.RSTB(1'b0), // 1-bit input: Reset input for BREG
		.RSTC(1'b0), // 1-bit input: Reset input for CREG
		.RSTCTRL(1'b0), // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
		.RSTD(1'b0), // 1-bit input: Reset input for DREG and ADREG
		.RSTM(1'b0), // 1-bit input: Reset input for MREG
		.RSTP(1'b0) // 1-bit input: Reset input for PREG
	);

endmodule
